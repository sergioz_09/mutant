package magneto


import mutant.SaveCommand

class MutantController {
	static responseFormats = ['json']
    MutantService mutantService
	
    def save(SaveCommand command) {
        Postulant postulant = mutantService.save(command)
        respond([:], status: (postulant.isMutant)? 200 : 403)
    }

    def stats() {
        respond(mutantService.stats())
    }
}
