package magneto

class UrlMappings {

    static mappings = {
        post "/mutant"(action:"save", controller: 'mutant')
        get "/stats"(action:"stats", controller: 'mutant')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
