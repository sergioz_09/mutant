package magneto

class Postulant {
    String dna
    Integer size
    Boolean isMutant
    static constraints = {
        size nullable: false
        isMutant nullable: false
        dna unique: true
    }
}
