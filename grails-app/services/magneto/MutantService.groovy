package magneto

import grails.gorm.transactions.Transactional
import mutant.SaveCommand

@Transactional
class MutantService {
    // Minimum of mutants sequences
    final Integer MIN_SEQUENCE = 1
    // Longitude to determine a mutant adn
    final Integer SEQUENCE_LENGTH = 4
    // Constants used in mutant checkers
    final Map COORDINATES = [
        vertical  : [x: 0, y: 1],
        horizontal: [x: 1, y: 0],
        lOblique  : [x: -1, y: 1],
        rOblique  : [x: 1, y: 1]
    ]

    /**
     * Save a Postulant (Unique by dna) and check if is a mutant
     * @param command
     * @return
     */
    Postulant save(SaveCommand command) {
        Map params = command.params()
        Postulant postulant = Postulant.findByDna(params.dna)
        if (!postulant) {
            postulant = new Postulant(params)
            postulant.isMutant = isMutant(command.dna)
            postulant.save()
        }
        return postulant
    }

    /**
     * Get total of mutant-postulant and get ratio
     * @return
     */
    Map stats() {
        Map rsp = [
            count_mutant_dna: Postulant.countByIsMutant(true),
            count_human_dna : Postulant.count()
        ]
        rsp.ratio = rsp.count_mutant_dna / rsp.count_human_dna
        return rsp
    }

    /**
     * Determine if a dna sequence belong to a mutant
     * @param dna
     * @return
     */
    boolean isMutant(List<String> dna) {
        int size = dna.size
        int mutantCount = 0

        if(size < SEQUENCE_LENGTH || !isSquare(dna)) {
            return false
        }

        // Coordinates used in a sequence
        Set<String> coordinatesInUse = []

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                char ch = dna[y][x]
                if (!coordinatesInUse.find { it == "$x$y" }) {
                    Map rsp = [found: false]

                    // Horizontal check
                    if (xLessCheck(x, size)) {
                        rsp = checkAdn("horizontal", ch, y, x, dna, coordinatesInUse)
                    }

                    // Vertical check
                    if (!rsp.found && yLessCheck(y, size)) {
                        rsp = checkAdn("vertical", ch, y, x, dna, coordinatesInUse)
                    }

                    // Left oblique check
                    if (!rsp.found && yLessCheck(y, size) && xGrantCheck(x)) {
                        rsp = checkAdn("lOblique", ch, y, x, dna, coordinatesInUse)
                    }

                    // Right oblique check
                    if (!rsp.found && yLessCheck(y, size) && xLessCheck(x, size)) {
                        rsp = checkAdn("rOblique", ch, y, x, dna, coordinatesInUse)
                    }

                    // If a sequence is found increment variable 'muntantCount' and check if is a mutant
                    if (rsp.found) {
                        mutantCount++
                        if (mutantCount > MIN_SEQUENCE) {
                            return true
                        }
                        // Set coordinates in use
                        coordinatesInUse.addAll(rsp.coordinates)
                    }
                }
            }
        }
        return false
    }

    /**
     * Following a direction determine if is a mutant sequence
     * @param direction Direction to check
     * @param ch Character to check
     * @param y Current "Y" position
     * @param x Current "X" position
     * @param dna A dna structure
     * @return A Map [found: true/false: coordinates: ['11', '22', ... 'xy' ]]
     */
    private Map checkAdn(String direction, char ch, int y, int x, List<String> dna, Set<String> coordinatesInUse) {
        Set<String> coordinates = ["$x$y"]
        x = x + COORDINATES[direction].x
        y = y + COORDINATES[direction].y
        int count = 1
        while (count < SEQUENCE_LENGTH && ch == dna[y][x] && !coordinatesInUse.find { it == "$x$y" }) {
            coordinates << "$x$y"
            x += COORDINATES[direction].x
            y += COORDINATES[direction].y
            count++
        }
        return [
            found      : count == SEQUENCE_LENGTH,
            coordinates: coordinates
        ]
    }

    private Boolean yLessCheck(y, size) {
        return y < size - (SEQUENCE_LENGTH - 1)
    }

    private Boolean xLessCheck(x, size) {
        return x < size - (SEQUENCE_LENGTH - 1)
    }

    private Boolean xGrantCheck(x) {
        return x > (SEQUENCE_LENGTH - 1)
    }

    /**
     * Check if a matrix is square
     * @param dna
     * @return
     */
    private Boolean isSquare(List<String> dna) {
        int size = dna.size
        Boolean isSquare = true
        int i = 0
        while (i < size && isSquare) {
            isSquare = dna[i].length() == size
            i++
        }
        return isSquare
    }
}
