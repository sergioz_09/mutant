package magneto

import grails.testing.mixin.integration.Integration
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

@Integration(applicationClass = Application.class)
class MutantServiceSpec extends Specification implements ServiceUnitTest<MutantService>{
    def setup() {
    }

    def cleanup() {
    }

    void "test valid dna mutant"() {
        when:
        List<String> dna1 = [
            "ATGCGA",
            "CAGTGC",
            "TTATGT",
            "AGAAGG",
            "CCCCTA",
            "TCACTG"
        ]
        then:
        service.isMutant(dna1) == true

        when:
        List<String> dna2 = [
            "TTGCGA",
            "CAGTGC",
            "TTATGT",
            "AGAAGG",
            "CCCCTA",
            "TCACTG"
        ]
        then:
        service.isMutant(dna2) == true

        when:
        List<String> dna3 = [
            "TTGCAA",
            "CAGTGC",
            "TTTTCT",
            "AGACGG",
            "CACCTA",
            "TCACTG"
        ]

        then:
        service.isMutant(dna3) == true

        when: "2 sequences in a 8x8 matrix"
        List<String> dna4 = [
            "TTGCAAAT",
            "CAGTGCTG",
            "TTTTCTGA",
            "AGACGGGT",
            "CACCTACT",
            "TCACTGAG",
            "TCACTGAG",
            "TCACTGAG"
        ]

        then:
        service.isMutant(dna4) == true

    }

    void "no mutant cases"() {
        when: "No sequeces"
        List<String> dna1 = [
            "ATGCGA",
            "CAGTGC",
            "TTATTT",
            "AGACGG",
            "GCGTCA",
            "TCACTG"
        ]
        then:
        service.isMutant(dna1) == false

        when: "Only one sequence"
        List<String> dna2 = [
            "AAAAAA",
            "CAGTGC",
            "TTATTT",
            "AGACGG",
            "GCGTCA",
            "TCACTG"
        ]
        then:
        service.isMutant(dna2) == false

        when: "No sequences"
        List<String> dna3 = [
            "AAAA",
            "CAGT",
            "TTAT",
            "AGAC"
        ]
        then:
        service.isMutant(dna3) == false
    }

    void "no square matrix"() {
        when:
        List<String> dna1 = [
            "ATGCGA",
            "CAGTGC"
        ]
        then:
        service.isMutant(dna1) == false

        when:
        List<String> dna2 = [
            "AAAAAA",
            "CAGTGCAAAA",
            "TTATTT",
            "AGA",
            "GCGTCA",
            "TCACTG"
        ]
        then:
        service.isMutant(dna2) == false
    }

    void "n less to min sequense"() {
        when:
        List<String> dna1 = [
            "AA",
            "CA"
        ]
        then:
        service.isMutant(dna1) == false
    }

    void "Using a gen already in use"() {
        when:
        List<String> dna1 = [
            "AAAA",
            "TGGA",
            "TCGA",
            "CGGA"
        ]
        then:
        service.isMutant(dna1) == false

        when:
        List<String> dna2 = [
            "CTAGGG",
            "TAGAAT",
            "TCAAAA",
            "CGGAAT",
            "CGGAAC",
            "CGGGTA"
        ]
        then:
        service.isMutant(dna2) == false
    }
}
