package mutant

import grails.validation.Validateable

class SaveCommand implements Validateable {
    List<String> dna

    Map params() {
        return [
            dna : dna.join(''),
            size: dna.size
        ]
    }
}
