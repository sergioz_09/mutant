# X-men Mercadolibre (Groovy and Grails) #

Este proyecto cuenta con la solución al problema de Magneto

### Detalles de Proyecto ###
* grails = 3.3.8
* gradle = 3.5

### Método isMutant()  ##
Ubicación: /grails-app/services/magneto/MutantService.groovy

### API REST URL ###
url: http://magneto-env.pwgr6ypqq2.us-east-1.elasticbeanstalk.com/

### Servicios ###

#### /mutant ####
Verifica y guarda en la base de datos una cadena de adn.

**uri**: /mutant

**type**: POST

**Content-type**: application/json

**body params example**:

    {
        “dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
    }
    
**Respond**:

status: 200 / 403

En caso de verificar un mutante, responde HTTP 200-OK, en caso contrario 403-Forbidden
    
#### /stats ####
Estadísticas de las verificaciones de ADN

**uri**: /stats

**type**: GET

**Content-type**: application/json
    
**Respond**:
    
    {“count_mutant_dna”:40, “count_human_dna”:100: “ratio”:0.4}

### TEST ###

Hay una series de test de unidad sobre el servicio "MutantService"

Ubicacion: grails-app/services/magneto/MutantService.groovy

    ./grailsw test-app
    
#### Ejecución local ####
* Instalar **Mysql** y crear una base de datos con el nombre **magneto**

Ejecutar: 

    ./grailsw run-app


